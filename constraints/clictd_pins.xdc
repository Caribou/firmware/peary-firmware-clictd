
#set_clock_groups -asynchronous -group FCLK_CLK0

set_property IOSTANDARD LVDS_25 [get_ports chip_readout_p]
set_property IOSTANDARD LVDS_25 [get_ports chip_readout_n]
set_property PACKAGE_PIN V28 [get_ports chip_readout_p]
set_property PACKAGE_PIN V29 [get_ports chip_readout_n]

set_property IOSTANDARD LVDS_25 [get_ports chip_pwren_p]
set_property IOSTANDARD LVDS_25 [get_ports chip_pwren_n]
set_property PACKAGE_PIN R28 [get_ports chip_pwren_p]
set_property PACKAGE_PIN T28 [get_ports chip_pwren_n]

set_property IOSTANDARD LVDS_25 [get_ports chip_tpulse_p]
set_property IOSTANDARD LVDS_25 [get_ports chip_tpulse_n]
set_property PACKAGE_PIN R25 [get_ports chip_tpulse_p]
set_property PACKAGE_PIN R26 [get_ports chip_tpulse_n]

set_property IOSTANDARD LVDS_25 [get_ports chip_shutter_p]
set_property IOSTANDARD LVDS_25 [get_ports chip_shutter_n]
set_property PACKAGE_PIN T30 [get_ports chip_shutter_p]
set_property PACKAGE_PIN U30 [get_ports chip_shutter_n]

set_property IOSTANDARD LVDS_25 [get_ports chip_rstn_p]
set_property IOSTANDARD LVDS_25 [get_ports chip_rstn_n]
set_property PACKAGE_PIN P30 [get_ports chip_rstn_p]
set_property PACKAGE_PIN R30 [get_ports chip_rstn_n]

set_property PACKAGE_PIN AF20 [get_ports chip_clk_p]
set_property PACKAGE_PIN AG20 [get_ports chip_clk_n]
set_property IOSTANDARD LVDS_25 [get_ports chip_clk_p]
set_property IOSTANDARD LVDS_25 [get_ports chip_clk_n]
set_property DIFF_TERM true [get_ports chip_clk_*]

set_property PACKAGE_PIN AE22 [get_ports clk_100_p]
set_property PACKAGE_PIN AF22 [get_ports clk_100_n]
set_property IOSTANDARD LVDS_25 [get_ports clk_100_p]
set_property IOSTANDARD LVDS_25 [get_ports clk_100_n]
set_property DIFF_TERM true [get_ports clk_100_*]

set_property PACKAGE_PIN AF23 [get_ports chip_data_p]
set_property PACKAGE_PIN AF24 [get_ports chip_data_n]
set_property IOSTANDARD LVDS_25 [get_ports chip_data_p]
set_property IOSTANDARD LVDS_25 [get_ports chip_data_n]
set_property DIFF_TERM true [get_ports chip_data_*]

set_property PACKAGE_PIN AF19 [get_ports chip_enable_p]
set_property PACKAGE_PIN AG19 [get_ports chip_enable_n]
set_property IOSTANDARD LVDS_25 [get_ports chip_enable_p]
set_property IOSTANDARD LVDS_25 [get_ports chip_enable_n]
set_property DIFF_TERM true [get_ports chip_enable_*]

#T0 and clock for SPIDR telescope
set_property PACKAGE_PIN AA22 [get_ports t0_p]
set_property PACKAGE_PIN AA23 [get_ports t0_n]
set_property IOSTANDARD LVDS_25 [get_ports t0_p]
set_property IOSTANDARD LVDS_25 [get_ports t0_n]

#set_property IOSTANDARD LVDS_25 [get_ports CLK_TLU_P]
#set_property PACKAGE_PIN AG21 [get_ports CLK_TLU_P]
#set_property PACKAGE_PIN AH21 [get_ports CLK_TLU_N]
#set_property IOSTANDARD LVDS_25 [get_ports CLK_TLU_N]

set_property PACKAGE_PIN AH23 [get_ports tlu_trig_p]
set_property PACKAGE_PIN AH24 [get_ports tlu_trig_n]
set_property IOSTANDARD LVDS_25 [get_ports tlu_trig_p]
set_property IOSTANDARD LVDS_25 [get_ports tlu_trig_n]
set_property DIFF_TERM true [get_ports tlu_trig_*]

#Clock loopback for TLU
set_property PACKAGE_PIN AG21 [get_ports FMC_TLU_CLK_P]
set_property PACKAGE_PIN AH21 [get_ports FMC_TLU_CLK_N]
set_property DIFF_TERM true [get_ports FMC_TLU_CLK_*]
set_property IOSTANDARD LVDS_25 [get_ports FMC_TLU_CLK_*]

#set_property DIFF_TERM true [get_ports FMC_TLU_CLK_*]
set_property IOSTANDARD LVDS_25 [get_ports FMC_REFCLK_*]
set_property PACKAGE_PIN U25 [get_ports FMC_REFCLK_P]
set_property PACKAGE_PIN V26 [get_ports FMC_REFCLK_N]

#set_property PACKAGE_PIN AD18 [get_ports ext_trig]
#set_property IOSTANDARD LVCMOS25 [get_ports ext_trig]

set_property PACKAGE_PIN AD18 [get_ports J67_out]
set_property IOSTANDARD LVCMOS25 [get_ports J67_out]

set_property PACKAGE_PIN AD19 [get_ports J68_out]
set_property IOSTANDARD LVCMOS25 [get_ports J68_out]

set_property PACKAGE_PIN AC19 [get_ports chip_clk_dbg]
set_property IOSTANDARD LVCMOS25 [get_ports chip_clk_dbg]
set_property PACKAGE_PIN AJ21 [get_ports chip_data_dbg]
set_property IOSTANDARD LVCMOS25 [get_ports chip_data_dbg]
set_property PACKAGE_PIN AK21 [get_ports chip_enable_dbg]
set_property IOSTANDARD LVCMOS25 [get_ports chip_enable_dbg]
set_property PACKAGE_PIN AB21 [get_ports chip_readout_dbg]
set_property IOSTANDARD LVCMOS25 [get_ports chip_readout_dbg]
set_property PACKAGE_PIN AB16 [get_ports chip_pwren_dbg]
set_property IOSTANDARD LVCMOS25 [get_ports chip_pwren_dbg]
set_property PACKAGE_PIN Y20 [get_ports chip_tpulse_dbg]
set_property IOSTANDARD LVCMOS25 [get_ports chip_tpulse_dbg]
set_property PACKAGE_PIN AA20 [get_ports chip_shutter_dbg]
set_property IOSTANDARD LVCMOS25 [get_ports chip_shutter_dbg]
set_property PACKAGE_PIN AC18 [get_ports chip_rstn_dbg]
set_property IOSTANDARD LVCMOS25 [get_ports chip_rstn_dbg]

create_clock -period 25.000 -name chip_clk_p -waveform {0.000 12.500} [get_ports chip_clk_p]
#create_clock -period 4.000 -name sys_diff_clock_clk_p -waveform {0.000 2.000} [get_ports sys_diff_clock_clk_p]
#create_clock -period 10.000 -name clk_100_p -waveform {0.000 5.000} [get_ports clk_100_p]
#create_clock -period 10.000 -name clk100ps  -waveform {0.000 5.000} [get_nets -hierarchical clk100ps_clk_shift]
set_clock_groups -asynchronous -group chip_clk_p
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks sys_diff_clock_clk_p]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks clk_100_p]
set_clock_groups -asynchronous -group clk100ps_clk_shift_1
set_clock_groups -asynchronous -group clk100ps_clk_shift

#set_false_path -from [get_clocks sys_diff_clock_clk_p] -to [get_clocks clk100ps_clk_shift_1]
#set_false_path -from [get_clocks sys_diff_clock_clk_p] -to [get_clocks clk100ps_clk_shift]
#set_false_path -from [get_clocks sys_diff_clock_clk_p] -to [get_clocks clk_100_p]
#set_false_path -from [get_clocks sys_diff_clock_clk_p] -to [get_clocks chip_clk_p]

#set_input_delay -clock [get_clocks chip_clk_p] -clock_fall -min -add_delay 5.000 [get_ports chip_data_n]
#set_input_delay -clock [get_clocks chip_clk_p] -clock_fall -max -add_delay 20.000 [get_ports chip_data_n]
#set_input_delay -clock [get_clocks chip_clk_p] -clock_fall -min -add_delay 5.000 [get_ports chip_data_p]
#set_input_delay -clock [get_clocks chip_clk_p] -clock_fall -max -add_delay 20.000 [get_ports chip_data_p]
#set_input_delay -clock [get_clocks chip_clk_p] -clock_fall -min -add_delay 5.000 [get_ports chip_enable_n]
#set_input_delay -clock [get_clocks chip_clk_p] -clock_fall -max -add_delay 20.000 [get_ports chip_enable_n]
#set_input_delay -clock [get_clocks chip_clk_p] -clock_fall -min -add_delay 5.000 [get_ports chip_enable_p]
#set_input_delay -clock [get_clocks chip_clk_p] -clock_fall -max -add_delay 20.000 [get_ports chip_enable_p]

set_false_path -from [get_ports chip_data_n]
set_false_path -from [get_ports chip_data_p]
set_false_path -from [get_ports chip_enable_n]
set_false_path -from [get_ports chip_enable_p]
#set_false_path -from [get_ports ext_trig]
set_false_path -from [get_ports t0_p]
set_false_path -from [get_ports t0_n]
set_false_path -from [get_ports tlu_trig_p]
set_false_path -from [get_ports tlu_trig_n]

set_false_path -to [get_ports chip_pwren_n]
set_false_path -to [get_ports chip_pwren_p]
set_false_path -to [get_ports chip_readout_n]
set_false_path -to [get_ports chip_readout_p]
set_false_path -to [get_ports chip_rstn_n]
set_false_path -to [get_ports chip_rstn_p]
set_false_path -to [get_ports chip_shutter_n]
set_false_path -to [get_ports chip_shutter_p]
set_false_path -to [get_ports chip_tpulse_n]
set_false_path -to [get_ports chip_tpulse_p]

#set_false_path -to [get_ports chip_clk_dbg]
set_false_path -to [get_ports chip_data_dbg]
set_false_path -to [get_ports chip_enable_dbg]

set_false_path -to [get_ports chip_readout_dbg]
set_false_path -to [get_ports chip_pwren_dbg]
set_false_path -to [get_ports chip_tpulse_dbg]
set_false_path -to [get_ports chip_shutter_dbg]
set_false_path -to [get_ports chip_rstn_dbg]



set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]
