`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.08.2019 13:33:01
// Design Name: 
// Module Name: clkcheck
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clkcheck(
    input clk40,
    input clk250,
    input arst,
    output rise,
    output fall,
    output noclk
  );

  logic reset;
  logic clk40_sync250;
  logic clk40_sync250_prev;
  logic miss_s;
  logic fall_s;
  logic rise_s;
  logic edge_s;
  logic [1:0] cnt_miss;
  logic miss_set;
  logic miss_clr;
  logic [2:0] cnt_restore;

  assign rise_s = !clk40_sync250_prev && clk40_sync250;
  assign fall_s = clk40_sync250_prev && !clk40_sync250;
  
  always_ff @(posedge clk250) begin
    clk40_sync250_prev <= clk40_sync250;
  end
  
  assign noclk = miss_s;
  assign rise = rise_s;
  assign fall = fall_s;

  sync_reset #(
    .NEGATIVE(0),
    .STAGES(6) 
  ) sync_rst_clkcheck (
    .clk(clk250),
    .arst_in(arst),  // i, async 
    .rst_out(reset)   // o, sync to clk
  );

  sync_signal #(
      .WIDTH(1),
      .STAGES(2) 
  ) slowclk_sync_inst (
      .out_clk(clk250),
      .signal_in(clk40),  // i, async 
      .signal_out(clk40_sync250)  // o, sync to out_clk
  );
  
  
  always_ff @(posedge clk250) begin
    if (reset || rise_s || fall_s) begin
      cnt_miss <= 'b0;
      miss_set <= 1'b0;
    end
    else if ( &cnt_miss ) begin
      cnt_miss <= cnt_miss;
      miss_set <= 1'b1;
    end
    else begin
      cnt_miss <= cnt_miss + 1;
      miss_set <= 1'b0;
    end
  end  
  
  always_ff @(posedge clk250) begin
    if (reset)
      miss_s <= 'b0;
    else if (miss_set)
      miss_s <= 1'b1;
    else if (miss_clr)
      miss_s <= 'b0;
    else
      miss_s <= miss_s;
  end
  
  always_ff @(posedge clk250) begin
    if (reset || miss_set) begin
      cnt_restore <= 'b0;
      miss_clr <= 1'b0;
    end
    else if ( &cnt_restore ) begin
      cnt_restore <= cnt_restore;
      miss_clr <= 1'b1;
    end
    else if (rise_s || fall_s) begin
      cnt_restore <= cnt_restore + 1;
      miss_clr <= 'b0;
    end
    else begin
      cnt_miss <= cnt_miss;
      miss_clr <= miss_clr;
    end
      
    
  end
  
  
  
  
endmodule