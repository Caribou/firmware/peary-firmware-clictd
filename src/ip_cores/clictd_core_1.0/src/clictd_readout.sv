`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.12.2019 13:13:41
// Design Name: 
// Module Name: clictd_readout
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


  module clictd_readout (
    // clk clock domain
    input  fifo_clk,
    input  rst,
    output [63 : 0] fifo_data_out,
    output fifo_data_out_valid,
    input  fifo_data_read,
    
    // chip_clk clock domain    
    output [15:0] frame_bitcount,
    output data_readout_end,
    output fifo_ovf,
    output readout_idle,
    input  readout_start,
    input  dummy_readout_start,
    
    input  chip_clk,
    input  chip_data,
    input  chip_enable,

    output chip_readout
  );
    
  (* ASYNC_REG = "true", keep = "true", shreg_extract = "no" *) logic chip_data_sync [2:0];
  (* ASYNC_REG = "true", keep = "true", shreg_extract = "no" *) logic chip_enable_sync [2:0];
  logic chip_data_i;
  logic chip_enable_i;
  logic chip_readout_o;

  logic [63 : 0] fifo_data_pointer;
  logic [63 : 0] fifo_data_in;
  logic [15 : 0] rd_frame_bitcount;
  logic fifo_data_in_valid;
  logic fifo_empty;
  logic fifo_full;
  logic fifo_ovf_latch;
  logic readout_end_o;
  logic readout_idle_o;
  logic pointer_shift;
  logic count_bit;
  logic pointer_rst;
  logic last_bit;
  logic fifo_data_in_write;
  
  typedef enum	{IDLE, START, START_DUMMY, ENABLE, ENABLE_DUMMY, FINISH} STATE_READOUT;
  STATE_READOUT rd_state, rd_state_next;
  
  assign fifo_data_out_valid = !fifo_empty;
  assign chip_readout = chip_readout_o;
  assign fifo_ovf = fifo_ovf_latch;
  assign data_readout_end = readout_end_o;
  assign readout_idle = readout_idle_o;
  assign frame_bitcount = rd_frame_bitcount;
    
  // capture input from chip synchronously to chip_clk
  assign chip_data_i   = chip_data_sync[2];
  assign chip_enable_i = chip_enable_sync[2];
  //
  always @(posedge chip_clk) begin
    chip_data_sync[2]   <= chip_data_sync[1];
    chip_enable_sync[2] <= chip_enable_sync[1];
  end
  //
  always @(negedge chip_clk) begin
    chip_data_sync[1]   <= chip_data_sync[0];
    chip_data_sync[0]   <= chip_data;
    chip_enable_sync[1] <= chip_enable_sync[0];
    chip_enable_sync[0] <= chip_enable;
  end


  // readout FSM state transition
  always_ff @(posedge chip_clk)
    if (rst)
      rd_state <= IDLE;
    else 
      rd_state <= rd_state_next;

   // readout FSM next state logic
  always_comb begin   
    // default is to stay in current state
    rd_state_next = rd_state;
    case (rd_state)
      IDLE : begin
        if (readout_start)
          rd_state_next = START;
        else if (dummy_readout_start)
          rd_state_next = START_DUMMY;
        else
          rd_state_next = IDLE;
      end
      START : begin
        if (chip_enable_i)
          rd_state_next = ENABLE;
        else
          rd_state_next = START;
      end
      START_DUMMY : begin
        if (chip_enable_i)
          rd_state_next = ENABLE_DUMMY;
        else
          rd_state_next = START_DUMMY;
      end
      ENABLE : begin
        if (chip_enable_i)
          rd_state_next = ENABLE;
        else
          rd_state_next = FINISH;
      end
      ENABLE_DUMMY : begin
        if (chip_enable_i)
          rd_state_next = ENABLE_DUMMY;
        else
          rd_state_next = IDLE;
      end
      FINISH : begin
        if (readout_start)
          rd_state_next = FINISH;
        else
          rd_state_next = IDLE;
      end
      default : begin
        rd_state_next = IDLE;
      end
    endcase  
  end

  // readout FSM outputs
  always_comb begin
    // defaults
    chip_readout_o  = 1'b0;
    pointer_shift   = 1'b0;
    count_bit       = 1'b0;
    pointer_rst     = 1'b0;
    last_bit        = 1'b0;
    readout_end_o   = 1'b0;
    readout_idle_o  = 1'b0;

    case (rd_state)
      IDLE : begin
        chip_readout_o  = 1'b0;
        pointer_shift   = 1'b0;
        count_bit       = 1'b0;
        last_bit        = 1'b0;
        readout_end_o   = 1'b0;
        if (dummy_readout_start || readout_start) begin
          pointer_rst     = 1'b1;
          readout_idle_o  = 1'b0;
        end
        else begin
          pointer_rst     = 1'b0;
          readout_idle_o  = 1'b1;
        end
      end

      START : begin
        chip_readout_o  = 1'b1;
        last_bit        = 1'b0;
        readout_end_o   = 1'b0;
        pointer_rst     = 1'b0;
        readout_idle_o  = 1'b0;
        if (chip_enable_i) begin
          pointer_shift = 1'b1;
          count_bit     = 1'b1;
        end
        else begin
          pointer_shift = 1'b0;
          count_bit     = 1'b0;
        end
      end

      START_DUMMY : begin
        chip_readout_o  = 1'b1;
        last_bit        = 1'b0;
        readout_end_o   = 1'b0;
        pointer_rst     = 1'b0;
        readout_idle_o  = 1'b0;
        pointer_shift   = 1'b0;
        if (chip_enable_i) begin
          count_bit     = 1'b1;
        end
        else begin
          count_bit     = 1'b0;
        end
      end

      ENABLE : begin
        readout_idle_o  = 1'b0;
        pointer_rst     = 1'b0;
        // data is coming
        if (chip_enable_i) begin
          chip_readout_o  = 1'b1;
          last_bit        = 1'b0;
          readout_end_o   = 1'b0;
          pointer_shift   = 1'b1;
          count_bit       = 1'b1;
        end
        // readout finished
        else begin
          chip_readout_o  = 1'b0;
          last_bit        = 1'b1;
          readout_end_o   = 1'b1;
          pointer_shift   = 1'b0;
          count_bit       = 1'b0;
        end
      end
      
      ENABLE_DUMMY : begin
        readout_idle_o  = 1'b0;
        pointer_rst     = 1'b0;
        pointer_shift   = 1'b0;
        last_bit        = 1'b0;
        readout_end_o   = 1'b0;
        // data is coming
        if (chip_enable_i) begin
          chip_readout_o  = 1'b1;
          count_bit       = 1'b1;
        end
        // readout finished
        else begin
          chip_readout_o  = 1'b0;
          count_bit       = 1'b0;
        end
      end
      
      FINISH : begin
        readout_idle_o  = 1'b0;
        pointer_rst     = 1'b0;
        chip_readout_o  = 1'b0;
        last_bit        = 1'b0;
        pointer_shift   = 1'b0;
        count_bit       = 1'b0;
        readout_end_o   = 1'b1;
      end
      
    endcase  

  end

  assign fifo_data_in_valid = fifo_data_in_write || last_bit;
  
  always_ff @(posedge chip_clk) begin
    if (pointer_rst || rst) begin
      fifo_data_pointer[63] <= 1'b1;
      fifo_data_in_write    <= 1'b0;
    end
    else if (pointer_shift) begin
      fifo_data_pointer[63] <= fifo_data_pointer[0];
      fifo_data_in_write    <= fifo_data_pointer[0];
    end
    else begin 
      fifo_data_in_write    <= 1'b0;
    end
  end
  
  always_ff @(posedge chip_clk) begin
    if (pointer_rst || rst)
      fifo_data_in[63] <= 1'b0;
    else if (pointer_shift)
      if (fifo_data_pointer[63])
        fifo_data_in[63] <= chip_data_i;
  end

  generate
    for (genvar i=62; i >= 0; i=i-1) begin

      always_ff @(posedge chip_clk) begin
        if (pointer_rst || rst)
          fifo_data_pointer[i] <= 1'b0;
        else if (pointer_shift)
          fifo_data_pointer[i] <= fifo_data_pointer[i+1];
      end
      
      always_ff @(posedge chip_clk) begin
        if (pointer_rst || rst || fifo_data_in_write)
          fifo_data_in[i] <= 1'b0;
        else if (pointer_shift)
          if (fifo_data_pointer[i])
            fifo_data_in[i] <= chip_data_i;
      end
      
    end
  endgenerate

  // input frame bit counter
  always_ff @(posedge chip_clk) begin
    if (rst || pointer_rst) begin
      rd_frame_bitcount <= 0;
    end
    else if (count_bit) begin
      rd_frame_bitcount <= rd_frame_bitcount + 1;
    end 
    else begin 
      rd_frame_bitcount <= rd_frame_bitcount;
    end
  end

  always_ff @(posedge chip_clk) begin
    if (rst || readout_start)
      fifo_ovf_latch <= 1'b0;
    else if (fifo_data_in_valid && fifo_full)
      fifo_ovf_latch <= 1'b1;
  end

  readout_fifo readout_fifo_inst (
    .rst(rst),        // input wire rst
    .wr_clk(chip_clk),  // input wire wr_clk
    .rd_clk(fifo_clk),  // input wire rd_clk
    .din(fifo_data_in),        // input wire [31 : 0] din
    .wr_en(fifo_data_in_valid),    // input wire wr_en
    .rd_en(fifo_data_read),    // input wire rd_en
    .dout(fifo_data_out),      // output wire [31 : 0] dout
    .full(fifo_full),      // output wire full
    .empty(fifo_empty)    // output wire empty
  );
  
/*
  ila_readout readout_ila (
    .clk(chip_clk), // input wire clk
    
    .probe0(fifo_data_in_valid), // input wire [0:0]  probe0  
    .probe1(fifo_data_in), // input wire [63:0]  probe1 
    .probe2(fifo_data_pointer), // input wire [63:0]  probe2 
    .probe3(rd_frame_bitcount), // input wire [15:0]  probe3
    .probe4(readout_start), // input wire [0:0]  probe2  
    .probe5(chip_data_i), // input wire [0:0]  probe4 
    .probe6(chip_enable_i), // input wire [0:0]  probe5 
    .probe7(chip_readout_o), // input wire [0:0]  probe6 
    .probe8(fifo_empty), // input wire [0:0]  probe7 
    .probe9(fifo_full), // input wire [0:0]  probe8 
    .probe10(fifo_ovf_latch), // input wire [7:0]  probe9 
    .probe11(readout_end_o), // input wire [7:0]  probe9 
    .probe12(readout_idle_o), // input wire [7:0]  probe9 
    .probe13(pointer_shift), // input wire [7:0]  probe9 
    .probe14(pointer_rst), // input wire [7:0]  probe9 
    .probe15(last_bit), // input wire [7:0]  probe9 
    .probe16(fifo_data_in_write), // input wire [0:0]  probe10
    .probe17(count_bit) // input wire [0:0]  probe10
  );
*/
  
endmodule
