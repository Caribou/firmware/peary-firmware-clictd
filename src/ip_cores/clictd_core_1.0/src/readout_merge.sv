`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.09.2019 20:35:19
// Design Name: 
// Module Name: fw_clictd
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module readout_merge (
    input clk_100,
    input clk_axi,
    input arst,
    
    // FIFO output
    output [63:0] fifo_data_out,
    output fifo_data_valid,
    //input  fifo_data_read,
    input  fifo_data_read_msb,
    input  fifo_data_read_lsb,
    output readout_busy,
    output readout_active,
    input fifo_status_read,
    
    // timestamp inputs - sync to clk_100
    input t0,
    input [7:0] ts_signal_in,

    // timestamps control and config
    // internally synced to clk_100
    input ts_conf_enable,
    input ts_conf_capture_enable,
    input [47:0] ts_conf_ts_init_value,
    input [15:0] ts_conf_signal_edges,

    // 1 pulse sync to clk_axi 
    input readout_start,
    input dummy_readout_start,
    
    // CHIP INTERFACE:
    input  chip_clk,
    input  chip_data,
    input  chip_enable,
    output chip_readout
    //output chip_rstn,
    // end CHIP INTERFACE

  );

  logic rst_axi;
  logic rst_100;
  logic rst_40;
  logic chip_readout_i;
  logic chip_readout_S100;
  
  logic [63:0] fifo_data_out_i;
  logic fifo_empty;
  logic fifo_full;
  logic fifo_data_read_i;
  logic fifo_data_read_msb_latch;
  logic fifo_data_read_lsb_latch;
  logic [63:0] fifo_data_in;
  logic fifo_data_in_valid;
  
  logic latch_counters;
  //logic readout_start_prev;
  //logic readout_start_rise;
  logic release_rd_start;
  //logic readout_start_Saxi;
  logic readout_start_latch;
  logic readout_start_Schip;
  logic readout_idle; 
  logic readout_idle_Saxi; 
  logic readout_end_Saxi;
  //logic readout_active; 
  logic readout_end;
  logic dummy_readout_start_Schip;

  logic readout_busy_i;
  logic ts_fifo_ovf_latch;
  logic [23:0] ts_frame_word_count;
  logic [23:0] ts_frame_words;
  logic ts_frame_word_zero;
  logic [63:0] ts_fifo_data;
  logic ts_fifo_read;
  logic ts_count_valid;
  logic ts_fifo_ovf;
  logic ts_fifo_data_valid;
  logic rd_merge_idle;
  
  logic rd_fifo_ovf_latch;
  logic [15:0] rd_frame_bitcount_latch;
  logic [15:0] rd_frame_bitcount;
  logic [10:0] rd_frame_word_count;
  logic [15:0] rd_words_total;
  logic rd_fifo_read;
  logic rd_frame_word_zero;
  logic [63:0] rd_fifo_data; 
  logic rd_fifo_data_valid;
  logic rd_fifo_ovf;
  
  assign chip_readout = chip_readout_i;

  
  ///////////////////////////
  //  RESET                //
  ///////////////////////////
  
  sync_reset #(
    .NEGATIVE(0),
    .STAGES(2) 
  ) sync_rst_clk100 (
    .clk(clk_100),
    .arst_in(arst),  // i, async 
    .rst_out(rst_100)   // o, sync to clk
  );  
  
  sync_reset #(
    .NEGATIVE(0),
    .STAGES(2) 
  ) sync_rst_clkaxi (
    .clk(clk_axi),
    .arst_in(arst),  // i, async 
    .rst_out(rst_axi)   // o, sync to clk
  );

  sync_reset #(
    .NEGATIVE(0),
    .STAGES(2) 
  ) sync_rst_clkcheck (
    .clk(chip_clk),
    .arst_in(arst),  // i, async 
    .rst_out(rst_40)   // o, sync to clk
  );
  
  ///////////////////////
  // BUSY              //
  ///////////////////////
  
  assign readout_busy = readout_busy_i;
  
  always_ff @(posedge clk_axi) begin
    if (rst_axi || fifo_empty)
      readout_busy_i <= 1'b0;
    else if (fifo_full)
      readout_busy_i <= 1'b1;
  end
    
  ///////////////////////
  // FIFO READ         //
  ///////////////////////
  
  /*
  generate
    if (AXI_64BIT_DATA = 1) begin
      assign fifo_data_out    = fifo_data_out_i;
      assign fifo_data_valid  = !fifo_empty;
      assign fifo_data_read_i = fifo_data_read;
    end 
    else begin
      logic fifo_read_msb;

      always_ff @(posedge S_AXI_ACLK) begin
        if (rst_axi)
          fifo_read_msb <= 1'b0;
        else if (fifo_data_read)
          fifo_read_msb <= ~fifo_read_msb;
      end

      assign fifo_data_out    = fifo_read_msb ? fifo_data_out_i[63:32] : fifo_data_out_i[31:0];
      assign fifo_data_valid  = !fifo_empty;
      assign fifo_data_read_i = fifo_data_read && fifo_read_msb;
      
    end
  endgenerate
  */
  
  assign fifo_data_out    = fifo_data_out_i;
  assign fifo_data_valid  = !fifo_empty;
  assign fifo_data_read_i = (fifo_data_read_msb_latch || fifo_data_read_msb) && (fifo_data_read_lsb_latch || fifo_data_read_lsb);
   
  always_ff @(posedge clk_axi) begin
    if (rst_axi || fifo_status_read || fifo_data_read_i) begin
      fifo_data_read_msb_latch <= 1'b0;
      fifo_data_read_lsb_latch <= 1'b0;
    end 
    else begin
      if (fifo_data_read_lsb) begin 
        fifo_data_read_lsb_latch <= 1'b1;
      end
      if (fifo_data_read_msb) begin 
        fifo_data_read_msb_latch <= 1'b1;
      end
    end
  end 
  
  
  ///////////////////////
  // MERGE FSM         //
  ///////////////////////
  
  typedef enum {IDLE, INIT, STORE_HEADER, STORE_TIMESTAMPS, STORE_RD_DATA} STATE_MERGE;
  STATE_MERGE mg_state, mg_state_next;
  
  // readout FSM state transition
  always_ff @(posedge clk_axi)
    if (rst_axi)
      mg_state <= IDLE;
    else 
      mg_state <= mg_state_next;

   // readout FSM next state logic
  always_comb begin   
    // default is to stay in current state
    mg_state_next = mg_state;
    case (mg_state)
      IDLE : begin
        if (readout_end_Saxi)
          mg_state_next = INIT;
        else
          mg_state_next = IDLE;
      end
      
      INIT : begin
        if (ts_count_valid)
          mg_state_next = STORE_HEADER;
        else
          mg_state_next = INIT;
      end
      
      STORE_HEADER : begin
        if (!fifo_full)
          mg_state_next = STORE_TIMESTAMPS;
        else
          mg_state_next = STORE_HEADER;
      end
      
      STORE_TIMESTAMPS : begin
          if (!ts_frame_word_zero) 
            mg_state_next = STORE_TIMESTAMPS;
          else
            mg_state_next = STORE_RD_DATA;
      end

      /*
      STORE_RD_BITCOUNT : begin
        if (!fifo_full)
          mg_state_next = STORE_RD_DATA;
        else
          mg_state_next = STORE_RD_BITCOUNT;
      end
      */

      STORE_RD_DATA : begin
        if (!rd_frame_word_zero)
          mg_state_next = STORE_RD_DATA;
        else
          mg_state_next = IDLE;
      end

      default : begin
        mg_state_next = IDLE;
      end
    endcase  
  end
  
  // readout FSM outputs
  always_comb begin
    // defaults
    latch_counters     = 1'b0;
    release_rd_start   = 1'b0;
    fifo_data_in       = 64'bx;
    fifo_data_in_valid = 1'b0;
    ts_fifo_read       = 1'b0;
    rd_fifo_read       = 1'b0;
    rd_merge_idle      = 1'b0;
    
    case (mg_state)
      IDLE : begin
        if (readout_end_Saxi) begin
          latch_counters   = 1'b1;
          release_rd_start = 1'b1;
        end
        else begin
          rd_merge_idle    = 1'b1;
        end
      end

      INIT : begin
      end


      STORE_HEADER : begin
        fifo_data_in[63:63] = rd_fifo_ovf_latch;
        fifo_data_in[62:62] = ts_fifo_ovf_latch;
        fifo_data_in[61:56] = 6'b0; // reserved
        fifo_data_in[55:48] = 8'b0; // reserved
        fifo_data_in[47:32] = rd_frame_bitcount_latch;
        fifo_data_in[31:16] = ts_frame_word_count;
        fifo_data_in[15: 0] = rd_words_total;
        if (!fifo_full) begin
          fifo_data_in_valid  = 1'b1;
        end
      end
      
      STORE_TIMESTAMPS : begin
          fifo_data_in       = ts_fifo_data;
          if ((!ts_frame_word_zero) && (!fifo_full)) begin
            fifo_data_in_valid = 1'b1;
            ts_fifo_read       = 1'b1;
          end
          else begin
            fifo_data_in_valid = 1'b0;
            ts_fifo_read       = 1'b0;
          end
      end
      
      /*      
      STORE_RD_BITCOUNT : begin
        fifo_data_in[63:0]  = 64'bx;
        if (!fifo_full) begin
          fifo_data_in_valid = 1'b0;
        end
      end
      */

      STORE_RD_DATA : begin
          fifo_data_in       = rd_fifo_data;
          if ((!rd_frame_word_zero) && (!fifo_full)) begin
            fifo_data_in_valid = 1'b1;
            rd_fifo_read       = 1'b1;
          end
          else begin
            fifo_data_in_valid = 1'b0;
            rd_fifo_read       = 1'b0;
          end
      end

    endcase
  end

  // rd_frame_word_count, ts_frame_word_count are 64bit, i.e. 8-byte words
  // add 1 64b word (8 bytes) in the first header (ts_frame_word_count, ts_fifo_ovf_latch, rd_bytes_total)
  // and 1 64b word (8 bytes) in the second header (rd_frame_bitcount_latch, rd_fifo_ovf_latch)
  // and shift by 3 bits - each word is 8 bytes
  assign rd_words_total = (rd_frame_word_count + ts_frame_word_count);

  ///////////////////////
  // TIMESTAMPS        //
  ///////////////////////
  
  /*
  sync_signal #(
      .WIDTH(1),
      .STAGES(2)
  ) sync_chip_readout_S100 (
      .out_clk(clk_100),
      .signal_in(chip_readout_i),
      .signal_out(chip_readout_S100)
  );
  assign ts_signal_in = {ext_trigger, chip_readout_S100, tlu_trigger, chip_pwren, chip_tpulse, chip_shutter, 1'b0, t0};
  */
  
  always_ff @(posedge clk_axi) begin
    if (rst_axi) begin
      ts_frame_word_count <= 13'b0;
      ts_fifo_ovf_latch   <= 1'b0;
    end
    else if (ts_count_valid) begin
      ts_frame_word_count <= ts_frame_words;
      ts_fifo_ovf_latch   <= ts_fifo_ovf;
    end
    else if (ts_fifo_read) begin
      ts_frame_word_count <= ts_frame_word_count - 1;
    end
  end
  
  assign ts_frame_word_zero = ~|ts_frame_word_count;
    
  
  timestamps #(
    .TS_WIDTH(48),
    .TS_SIGNALS_N(8)
  ) timestamps_inst (
    .clk_ts(clk_100),
    .clk_cfg(clk_axi),
    .rst(rst_100),
    .t0(t0),
    .get_ts_count(latch_counters), // 1b i 
    .conf_enable(ts_conf_enable),
    .conf_capture_enable(ts_conf_capture_enable),
    .conf_ts_init_value(ts_conf_ts_init_value),
    .conf_signal_edges(ts_conf_signal_edges),
    .signals_in(ts_signal_in),
    .read_ts_fifo(ts_fifo_read),
    .ts_fifo_out(ts_fifo_data),
    .ts_count_valid(ts_count_valid), // 1b o 
    .ts_count(ts_frame_words), // 32b o 
    .ts_fifo_ovf(ts_fifo_ovf),
    .output_valid(ts_fifo_data_valid)
  ); 

  ///////////////////////
  // CHIP DATA READOUT //
  /////////////////////// 
  
  always_ff @(posedge clk_axi) begin
    if (rst_axi) begin
      rd_frame_bitcount_latch <= 16'b0;
      rd_frame_word_count     <= 10'b0;
      rd_fifo_ovf_latch       <= 1'b0;
    end
    else if (latch_counters) begin
      rd_frame_bitcount_latch <= rd_frame_bitcount;
      rd_frame_word_count     <= rd_frame_bitcount[15:6] + (|rd_frame_bitcount[5:0]);
      rd_fifo_ovf_latch       <= rd_fifo_ovf;
    end
    else if (rd_fifo_read) begin
      rd_frame_word_count     <= rd_frame_word_count - 1;
    end
  end
  
  assign rd_frame_word_zero = ~|rd_frame_word_count;  
   
  always_ff @(posedge clk_axi) begin
    if (rst_axi)
      readout_start_latch <= 1'b0;
    else if (readout_start)
      readout_start_latch <= 1'b1;
    else if (release_rd_start)
      readout_start_latch <= 1'b0;
  end

  assign readout_active = readout_start_latch || ~readout_idle_Saxi || ~rd_merge_idle;

  /*
  always_ff @(posedge clk_axi)
    readout_start_prev <= readout_start;
  
  assign readout_start_rise = !readout_start_prev && readout_start;
  */

  sync_signal #(
      .WIDTH(1),
      .STAGES(2)
  ) sync_level_chip (
      .out_clk(chip_clk),
      .signal_in(readout_start_latch),
      .signal_out(readout_start_Schip)
  );

  sync_pulse #(
      .WIDTH(1),
      .STAGES(2)
  ) sync_pulse_axi_to_chip (
      .in_clk(clk_axi),
      .out_clk(chip_clk),
      .pulse_in(dummy_readout_start),
      .pulse_out(dummy_readout_start_Schip)
  );

  sync_signal #(
      .WIDTH(2),
      .STAGES(2)
  ) sync_level_axi (
      .out_clk(clk_axi),
      .signal_in({readout_end, readout_idle}),
      .signal_out({readout_end_Saxi, readout_idle_Saxi})
  );

  
  clictd_readout clictd_readout_inst (
    // clk clock domain
    .fifo_clk(clk_axi),
    .rst(rst_40),
    .fifo_data_out(rd_fifo_data), // o [(OUTPUT_BYTES*8)-1 : 0]
    .fifo_data_out_valid(rd_fifo_data_valid), // o
    .fifo_ovf(rd_fifo_ovf),
    .fifo_data_read(rd_fifo_read),
    
    // chip_clk clock domain    
    .frame_bitcount(rd_frame_bitcount), // o [15:0] 
    .data_readout_end(readout_end), // o
    .readout_idle(readout_idle), // o
    .readout_start(readout_start_Schip),
    .dummy_readout_start(dummy_readout_start_Schip),
    .chip_clk(chip_clk),
    .chip_data(chip_data),
    .chip_enable(chip_enable),
    .chip_readout(chip_readout_i)
  );
  
  data_fifo data_fifo_inst (
    .clk(clk_axi),      // input wire clk
    .rst(rst_axi),      // input wire rst
    .din(fifo_data_in),      // input wire [63 : 0] din
    .wr_en(fifo_data_in_valid),  // input wire wr_en
    .rd_en(fifo_data_read_i),  // input wire rd_en
    .dout(fifo_data_out_i),    // output wire [63 : 0] dout
    .full(fifo_full),    // output wire full
    .empty(fifo_empty)  // output wire empty
  );
/*  
  ila_readout_merge ila_readout_merge_i (
    .clk(clk_axi), // input wire clk
    
    .probe0 (readout_start_latch), // input wire [:0]  probe0  
    .probe1 (latch_counters), // input wire [:0]  probe0  
    .probe2 (release_rd_start), // input wire [:0]  probe1 
    .probe3 (fifo_data_in_valid), // input wire [:0]  probe2 
    .probe4 (ts_fifo_read), // input wire [:0]  probe3 
    .probe5 (rd_fifo_read), // input wire [:0]  probe4 
    .probe6 (fifo_empty), // input wire [:0]  probe5 
    .probe7 (fifo_data_read_i), // input wire [:0]  probe6 
    .probe8 (fifo_full), // input wire [:0]  probe7 
    .probe9 (chip_readout_i), // input wire [:0]  probe8 
    .probe10 (readout_idle), // input wire [:0]  probe9 
    .probe11 (readout_active), // input wire [:0]  probe10 
    .probe12 (readout_end_Saxi), // input wire [:0]  probe11 
    .probe13 (readout_start), // input wire [:0]  probe12 
    .probe14 (readout_busy_i), // input wire [:0]  probe13 
    .probe15 (ts_fifo_ovf_latch), // input wire [:0]  probe14 
    .probe16 (ts_frame_word_zero),  // input wire [:0]  probe15 
    .probe17 (ts_count_valid),  // input wire [:0]  probe15 
    .probe18 (ts_fifo_ovf),  // input wire [:0]  probe15 
    .probe19 (ts_fifo_data_valid),  // input wire [:0]  probe15 
    .probe20 (rd_fifo_ovf_latch),  // input wire [:0]  probe15 
    .probe21 (rd_frame_word_zero),  // input wire [:0]  probe15 
    .probe22 (rd_fifo_data_valid),  // input wire [:0]  probe15 
    .probe23 (rd_fifo_ovf),  // input wire [:0]  probe15 
    .probe24 (ts_frame_word_count),  // input wire [23:0]  probe15 
    .probe25 (ts_frame_words),  // input wire [23:0]  probe15 
    .probe26 (rd_frame_bitcount_latch),  // input wire [15:0]  probe15 
    .probe27 (rd_frame_bitcount),  // input wire [15:0]  probe15 
    .probe28 (rd_frame_word_count),  // input wire [10:0]  probe15 
    .probe29 (rd_words_total),  // input wire [15:0]  probe15 
    .probe30 (fifo_data_in),  // input wire [63:0]  probe16 
    .probe31 (fifo_data_out_i),  // input wire [63:0]  probe16 
    .probe32 (rd_merge_idle),  // input wire [63:0]  probe16 
    .probe33 (rd_fifo_data)  // input wire [63:0]  probe16 
  );
*/
endmodule
